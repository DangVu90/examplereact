import React from 'react';
import MenuTest from './MenuTest.jsx';

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [
        {
          id: "lv_0_1",
          title: "Top level 1",
          slug: "top-level-1",
          parent_id: "",
          children: [
            {
              id: "lv_1_1",
              title: "Sub level 1",
              slug: "sub-level-1",
              parent_id: "lv_0_1",
              children: [
                {
                  id: "lv_2_1",
                  title: "Sub Sub Level 1",
                  slug: "sub-sub-level-1",
                  parent_id: "lv_1_1",
                  children: [
                    {
                      id: "lv_3_1",
                      title: "Sub Sub sub Level 1",
                      slug: "sub-sub-sub-level-1",
                      parent_id: "lv_2_1",

                    }
                  ]
                }
              ]
            },
            {
              id: "lv_1_2",
              title: "Sub level 2",
              slug: "sub-level-2",
              parent_id: "lv_0_1",
            }
          ]
        },
        {
          id: "lv_0_2",
          title: "Top level 2",
          slug: "top-level 2",
          parent_id: "",
        }
      ],
    }
  }
  render() {
    return (
      <div>simon, helloworld24353453!!!

        <MenuTest submenu={this.state.data} />
      </div>
    );
  }
}

export default App;
