import React from 'react';
// import SubMenu  from './SubMenu.jsx';
// import './MenuTest.css';

class MenuTest extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: 'id0',
			currentSubMenu: null,
			currentSubParent: null,
			currentSubParentName: null,
			selectedIds: [],
			has_child: true,
			status:false,
			active: '',
			parentID:''
			
		}
		this.handleClick = this.handleClick.bind(this);
		this.handleBack = this.handleBack.bind(this);
	}
	handleClick(item) {
		console.log('item:', item);
		this.setState({ 
			active: `${item.id}`,
			status: true,
			parentID: item.parent_id
		 });
	}
	handleBack(item) {
		
	}
	renderList(submenu){
		const { active, status, i } = this.state;
		return submenu.map((item, index) =>
			<li
				key={index}
				className={(this.handleClick && active === `${item.id}` && status ? 'active' : (this.state.parentID !='' ? 'hide': ''))}
			>
				<span onClick={() => this.handleClick(item)}>{item.title}</span>
				<MenuTest className={(this.handleClick && active === `${item.id}` && status ? 'active' : '')} submenu={item.children} />
			</li>
		)
	}
	render() {
		const { submenu } = this.props;
		if (Array.isArray(submenu)) {
			return (<ul className={this.props.className} >
				{this.renderList(submenu)}
			</ul>)
		}
		return null;
	}
}


export default MenuTest;