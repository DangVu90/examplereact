import React from 'react';
import MenuTest from './MenuTest.jsx';

class Submenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active:false,
		}
		this.handleClick = this.handleClick.bind(this);
	}
	handleClick(){
		this.setState({
			active: !this.state.active
		})
	}

	render() {
		let className = this.state.active ? 'active' : 'inactive';
		return (
			<li className={className} onClick={this.handleClick}>
				<span>{this.props.data}</span>
				<MenuTest submenu={this.props.submenu} />
			</li>
		)
	}
}


export default Submenu;